%include "lib.inc"

global find_word

section .text
find_word:
    .loop:
        push rdi                    
        push rsi                   
        add rsi, 8             
        call string_equals          
        pop rsi                    
        pop rdi
        cmp rax, 1                 
        je .yes                
        mov rsi, [rsi]              
        cmp rsi,0                
        je .no                  
        jmp .loop
    .yes:
        mov rax, rsi               
    .no: 
        ret

